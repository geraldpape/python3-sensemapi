# system modules
import unittest

# internal modules
from sensemapi.sensor import senseBoxSensor

# external modules

class senseBoxSensorBaseTest(unittest.TestCase):
    def setUp(self):
        self.sensor = senseBoxSensor()

class senseBoxSensorTest(senseBoxSensorBaseTest):
    def test_to_json_empty_spec_id(self):
        self.sensor.id = "7f65a87e6fa7e5fa76e56a5e"
        to_json = self.sensor.to_json()
        self.assertIn("_id",to_json)
        self.assertIn("title",to_json)
        self.assertIn("unit",to_json)
        self.assertIn("sensorType",to_json)
        self.assertIn("icon",to_json)

    def test_to_json_empty_no_id(self):
        to_json = self.sensor.to_json()
        self.assertNotIn("_id",to_json)
        self.assertIn("title",to_json)
        self.assertIn("unit",to_json)
        self.assertIn("sensorType",to_json)
        self.assertIn("icon",to_json)

    def test_to_json_without_id(self):
        to_json = self.sensor.to_json(with_id = False)
        self.assertNotIn("_id", to_json)

    def test_to_json_edited_no_id_raises(self):
        with self.assertRaises(AssertionError):
            self.sensor.to_json(edited = True)

    def test_to_json_edited(self):
        self.sensor.id = "7f65a87e6fa7e5fa76e56a5e"
        self.sensor.title = "title"
        self.sensor.unit = "unit"
        self.sensor.type = "type"
        self.sensor.icon = "icon"
        to_json = self.sensor.to_json(edited = True)
        self.assertTrue(to_json.get("title"))
        self.assertTrue(to_json.get("unit"))
        self.assertTrue(to_json.get("icon"))
        self.assertTrue(to_json.get("sensorType"))
        self.assertNotIn("new", to_json)
        self.assertNotIn("deleted", to_json)
        self.assertIs(to_json.get("edited"), True)

    def test_to_json_deleted(self):
        self.sensor.id = "7f65a87e6fa7e5fa76e56a5e"
        to_json = self.sensor.to_json(deleted = True)
        self.assertIs(to_json.get("deleted"), True)
        self.assertNotIn("edited", to_json)
        self.assertNotIn("new", to_json)

    def test_to_json_deleted_without_id_raises(self):
        with self.assertRaises(AssertionError):
            self.sensor.to_json(deleted = True)

    def test_to_json_new_empty_raises(self):
            with self.assertRaises(AssertionError):
                self.sensor.to_json(new = True)

    def test_to_json_new(self):
        self.sensor.title = "title"
        self.sensor.unit = "unit"
        self.sensor.type = "type"
        self.sensor.icon = "icon"
        to_json = self.sensor.to_json(new = True)
        self.assertTrue(to_json.get("title"))
        self.assertTrue(to_json.get("unit"))
        self.assertTrue(to_json.get("icon"))
        self.assertTrue(to_json.get("sensorType"))
        self.assertIs(to_json.get("edited"), True)
        self.assertIs(to_json.get("new"), True)
        self.assertNotIn("deleted", to_json)
        self.assertNotIn("_id", to_json)

    def test_to_json_deleted_edited_new_raises(self):
        with self.assertRaises(AssertionError):
            self.sensor.to_json(edited = True, new = True, deleted = True)
