# system modules
import os
import unittest

# internal modules
from sensemapi.client import SenseMapClient
from sensemapi import paths

# external modules
import pandas as pd

API_SERVER = os.environ.get("SENSEMAPI_TEST_API",paths.OPENSENSEMAP_API_TEST)

class SenseMapClientBaseTest(unittest.TestCase):
    def setUp(self):
        self.client = SenseMapClient(api = API_SERVER)


class SenseMapClientTest(SenseMapClientBaseTest):
    pass
