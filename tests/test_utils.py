# system modules
import unittest
from unittest.mock import patch, Mock
import datetime

# internal modules
from sensemapi.utils import *

# external modules

class SenseMapDateUtilsTest(unittest.TestCase):
    s = "2016-06-02T11:22:51.817Z"
    d = datetime.datetime(2016,6,2,11,22,51,817000,
        tzinfo = datetime.timezone.utc)

    def test_str2date(self):
        self.assertLess(
            (str2date(self.s) - self.d).total_seconds(),
            1)

    def test_str2date_date2str_consistency(self):
        self.assertLess(
            (str2date(date2str(self.d)) - self.d).total_seconds(),
            1)
