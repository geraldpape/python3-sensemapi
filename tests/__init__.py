# system modules
import os
import unittest

# internal modules

# external modules

# decorator to skip tests that require credentials
def needs_credentials(f):
    return unittest.skipUnless(
        os.environ.get("SENSEMAP_EMAIL") \
            and os.environ.get("SENSEMAP_PASSWORD"),
        "no credentials specified via SENSEMAP_EMAIL/SENSEMAP_PASSWORD "
            "environment variables"
        )(f)


