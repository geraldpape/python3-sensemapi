
Installation
============

:mod:`sensemapi` is best installed via ``pip``::

    pip3 install --user sensemapi
