Welcome to sensemapi's documentation!
===========================================

:mod:`sensemapi` is a Python package to access the `OpenSenseMap API
<https://api.opensensemap.org>`_.

.. note::

    Disclaimer
    ----------

    This software was developed within the context of a
    `CO2 monitoring project <https://gitlab.com/tue-umphy/co2mofetten>`_
    of the University of Tübingen, Germany. The developer is not in any
    way affiliated with the `senseBox project <https://www.sensebox.de/en/>`_.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   whatsthis
   install
   notebooks/basic-sensemapi-usage.rst
   api/modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
